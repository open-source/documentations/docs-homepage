# Homepage for IQRF Gateway documentations

![IQRF Gateway package repositories logo](./src/assets/logo.svg)


[![pipeline status](https://gitlab.iqrf.org/open-source/documentations/docs-homepage/badges/master/pipeline.svg)](https://gitlab.iqrf.org/open-source/documentations/docs-homepage/-/commits/master)

Homepage for IQRF Gateway documentations located at https://docs.iqrf.org/.

## Usage

### Development

Run the following commands to start the development server. The server will be available at http://localhost:5173/.

```bash
pnpm update
pnpm dev
```

### Build

Run the following commands to build the project. The build artifacts will be stored in the `dist/` directory.

```bash
pnpm update
pnpm build
```
