/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import path from 'node:path';
import { fileURLToPath, URL } from 'node:url';

import VueI18n from '@intlify/unplugin-vue-i18n/vite';
import UnheadVite from '@unhead/addons/vite';
import vue from '@vitejs/plugin-vue';
import { defineConfig, loadEnv, type UserConfig } from 'vite';
import Pages from 'vite-plugin-pages';
import VueDevTools from 'vite-plugin-vue-devtools';
import Layouts from 'vite-plugin-vue-layouts';
import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify';
import generateSitemap from 'vite-ssg-sitemap';

export default defineConfig(({ mode }) => {
	const env = loadEnv(mode, process.cwd(), '');
	return {
		base: '/',
		build: {
			outDir: path.resolve(__dirname, './dist'),
		},
		plugins: [
			VueDevTools({
				componentInspector: true,
				launchEditor: env.VITE_EDITOR,
			}),
			vue({
				template: { transformAssetUrls },
			}),
			vuetify({
				autoImport: {
					labs: true,
				},
			}),
			UnheadVite(),
			VueI18n({
				runtimeOnly: true,
				compositionOnly: true,
				fullInstall: true,
				include: [path.resolve(__dirname, 'src/locales/**')],
			}),
			Pages({
				extensions: ['vue', 'md'],
			}),
			Layouts(),
		],
		optimizeDeps: {
			exclude: ['vuetify'],
		},
		resolve: {
			alias: {
				'@': fileURLToPath(new URL('./src', import.meta.url)),
			},
			extensions: ['.js', '.json', '.jsx', '.mjs', '.ts', '.tsx', '.vue'],
		},
		ssgOptions: {
			script: 'async',
			formatting: 'minify',
			beastiesOptions: {
				reduceInlineStyles: false,
			},
			includeAllRoutes: true,
			onFinished(): void {
				generateSitemap();
			},
		},
		ssr: {
			noExternal: [/vue-i18n/, 'vuetify'],
		},
	} as UserConfig;
});
