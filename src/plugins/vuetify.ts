/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { createVuetify } from 'vuetify';
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg';
import * as labs from 'vuetify/labs/components';

import 'vuetify/styles';
import '@/styles/main.scss';

export default createVuetify({
	components: {
		...labs,
	},
	icons: {
		defaultSet: 'mdi',
		aliases,
		sets: {
			mdi,
		},
	},
	theme: {
		themes: {
			dark: {
				colors: {
					primary: '#367fa9',
					background: '#161618',
				},
			},
			light: {
				colors: {
					primary: '#367fa9',
					background: '#f6f6f7',
				},
			},
		},
	},
});
