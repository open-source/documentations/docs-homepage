/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { type App } from 'vue';

import head from '@/plugins/head';
import i18n from '@/plugins/i18n';
import vuetify from '@/plugins/vuetify';

/**
 * Registers plugins
 * @param {App} app Vue app
 */
export function registerPlugins(app: App): void {
	app.use(head);
	app.use(i18n);
	app.use(vuetify);
}
